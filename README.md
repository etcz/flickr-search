Flickr Search
=============

A simple app that that uses the Flickr image search API and shows the results in a 3-column scrollable view.

![Demo](demo.gif)

[Get the (debug) APK](https://bitbucket.org/etcz/flickr-search/src/master/flickr-search-debug.apk)

Implementation notes
--------------------
* View Architecture: MVVM.
* Primary language used: Kotlin.
* Project uses packaging-by-feature.
* Libraries used (some but not all): Dagger2, Retrofit, Gson, RxJava, Jetpack/AndroidX (ViewModel, LiveData), Material Components, and Glide.
* Rotation/configration changes and process death are all handled.
* UI flickers during the above mentioned scenarios/cases are also avoided.
* Error handling is **not** an afterthought.
* App architecture designed with extensibility in mind.
* ViewModel and (data) models have some unit test coverage.

Pagination support
------------------
This app has been extended enable endless scrolling.
Refer to: https://bitbucket.org/etcz/flickr-search-infinite-scroll/src/master/

Further improvements (given more time)
--------------------------------------
* Loading state and transitions (when loading the search results).
* Better image loading transitions (i.e. using Glide's [thumbnail requests](https://bumptech.github.io/glide/doc/options.html#thumbnail-requests)).
* Improved search UX (e.g. handling recent searches, nicer app bar transitions, etc)
* Pre-determining an image's dimension (its width and height) before to allow for rendering according to its natural aspect ratio.
* The above is possible by doing a `HEAD` request for an image's URL. Flickr's API returns with `ImageWidth` and `ImageHeight` values in the response headers.
* Using a a [StaggeredGridLayoutManager](https://developer.android.com/reference/kotlin/androidx/recyclerview/widget/StaggeredGridLayoutManager) to allow images to be displayed at their natural aspect ratio.