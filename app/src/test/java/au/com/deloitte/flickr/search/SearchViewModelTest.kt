package au.com.deloitte.flickr.search

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.Observer
import au.com.deloitte.flickr.BaseTest
import au.com.deloitte.flickr.common.FlickrPhoto
import com.nhaarman.mockito_kotlin.argumentCaptor
import com.nhaarman.mockito_kotlin.atLeastOnce
import com.nhaarman.mockito_kotlin.mock
import com.nhaarman.mockito_kotlin.verify
import io.reactivex.Single
import org.junit.Assert.assertEquals
import org.junit.Assert.assertNull
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.anyString
import java.io.IOException

class SearchViewModelTest : BaseTest() {

    @get:Rule
    val rule = InstantTaskExecutorRule()

    @Mock
    private lateinit var repository: SearchRepository
    private lateinit var searchViewModel: SearchViewModel
    private val observer: Observer<SearchViewState> = mock()
    private val photoListStub = listOf(
        FlickrPhoto(
            id = "1",
            secret = "1",
            server = "1",
            farm = 1
        ),
        FlickrPhoto(
            id = "2",
            secret = "2",
            server = "2",
            farm = 2
        )
    )

    @Before
    fun setup() {
        searchViewModel = SearchViewModel(repository, testSchedulers)
        searchViewModel.liveData.observeForever(observer)
    }

    @Test
    fun `given a search request, when there is no error upstream, then the correct list is returned`() {
        // given
        `when`(repository.searchPhotos(anyString()))
            .thenReturn(Single.just(photoListStub))

        // when
        searchViewModel.searchPhotos("query")
        testScheduler.triggerActions()

        // then
        argumentCaptor<SearchViewState>().run {
            verify(observer, atLeastOnce()).onChanged(capture())
            val photoItems = allValues.last().photoItems
            assertEquals(2, photoItems.size)
            assertEquals("1", photoItems[0].id)
            assertEquals("2", photoItems[1].id)
        }
    }

    @Test
    fun `given a search request, when there is no error upstream, then the correct sequence of events are observed`() {
        // given
        `when`(repository.searchPhotos(anyString()))
            .thenReturn(Single.just(photoListStub))

        // when
        searchViewModel.searchPhotos("query")
        testScheduler.triggerActions()

        // then
        argumentCaptor<SearchViewState>().run {
            verify(observer, atLeastOnce()).onChanged(capture())
            assertEquals(RequestState.INITIAL, allValues[0].requestState)
            assertEquals(RequestState.LOADING, allValues[1].requestState)
            assertEquals(RequestState.SUCCESS, allValues[2].requestState)
        }
    }

    @Test
    fun `given a search request, when there are invalid items in the upstream result, then those items are discarded`() {
        // given
        val invalidId = "^^"
        val photoWithInvalidElement = FlickrPhoto(
            id = invalidId,
            secret = "secret",
            server = "server",
            farm = 3
        )
        val listStubWithInvalidItem = photoListStub.plus(photoWithInvalidElement)
        `when`(repository.searchPhotos(anyString()))
            .thenReturn(Single.just(listStubWithInvalidItem))

        // when
        searchViewModel.searchPhotos("query")
        testScheduler.triggerActions()

        // then
        argumentCaptor<SearchViewState>().run {
            verify(observer, atLeastOnce()).onChanged(capture())
            val photoItems = allValues.last().photoItems
            assertEquals(2, photoItems.size)
            assertNull(photoItems.find { it.id == invalidId })
        }
    }

    @Test
    fun `given a search request, when there is an error upstream, then that error is propagated`() {
        // given
        val upstreamError = IOException("Canceled")
        val expected = SearchViewState(
            query = "query",
            error = upstreamError,
            requestState = RequestState.ERROR
        )
        `when`(repository.searchPhotos(anyString()))
            .thenReturn(Single.error(upstreamError))

        // when
        searchViewModel.searchPhotos("query")
        testScheduler.triggerActions()

        // then
        argumentCaptor<SearchViewState>().run {
            verify(observer, atLeastOnce()).onChanged(capture())
            assertEquals(expected, allValues.last())
        }
    }
}