package au.com.deloitte.flickr

import io.reactivex.schedulers.Schedulers
import io.reactivex.schedulers.TestScheduler
import org.junit.Before
import org.mockito.MockitoAnnotations

open class BaseTest {

    public val testScheduler = TestScheduler()

    protected val trampolineSchedulers = RxSchedulers(
        io = Schedulers.trampoline(),
        computation = Schedulers.trampoline(),
        main = Schedulers.trampoline()
    )

    protected val testSchedulers = RxSchedulers(
        io = testScheduler,
        computation = testScheduler,
        main = testScheduler
    )

    @Before
    fun baseSetup() {
        MockitoAnnotations.initMocks(this)
    }
}

