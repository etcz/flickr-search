package au.com.deloitte.flickr.search

import android.view.View
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import au.com.deloitte.flickr.R
import com.bumptech.glide.Glide

class PhotoItemViewHolder(
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val imageView by lazy { itemView.findViewById<ImageView>(R.id.photoImageView) }

    fun bind(item: PhotoItem) {
        Glide.with(itemView)
            .load(item.url)
            .centerCrop()
            .error(R.drawable.ic_broken_image)
            .into(imageView)
    }
}
