package au.com.deloitte.flickr.di

import au.com.deloitte.flickr.search.SearchActivity
import dagger.Subcomponent

@SearchScope
@Subcomponent(
    modules = [
        SearchModule::class,
        ViewModelModule::class
    ]
)
interface SearchComponent {

    @Subcomponent.Factory
    interface Factory {
        fun create(): SearchComponent
    }

    fun inject(activity: SearchActivity)
}