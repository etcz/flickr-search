package au.com.deloitte.flickr.di

import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AppModule::class,
        NetworkModule::class,
        SubcomponentsModule::class
    ]
)
interface AppComponent {
    fun searchComponent(): SearchComponent.Factory
}
