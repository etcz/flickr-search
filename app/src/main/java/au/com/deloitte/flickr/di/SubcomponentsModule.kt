package au.com.deloitte.flickr.di

import dagger.Module

@Module(
    subcomponents = [
        SearchComponent::class
    ]
)
class SubcomponentsModule {}