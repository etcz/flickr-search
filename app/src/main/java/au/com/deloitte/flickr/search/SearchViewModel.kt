package au.com.deloitte.flickr.search

import androidx.annotation.MainThread
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import au.com.deloitte.flickr.RxSchedulers
import au.com.deloitte.flickr.common.BaseViewModel
import au.com.deloitte.flickr.common.FlickrPhoto
import javax.inject.Inject

class SearchViewModel @Inject constructor(
    private val repository: SearchRepository,
    private val schedulers: RxSchedulers
) : BaseViewModel() {

    private var currentState = SearchViewState(requestState = RequestState.INITIAL)
    private val _liveData = MutableLiveData(currentState)
    val liveData: LiveData<SearchViewState> = _liveData

    fun searchPhotos(text: String) {
        currentState = currentState.copy(query = text)
        disposables.add(
            repository.searchPhotos(text)
                .subscribeOn(schedulers.io)
                .observeOn(schedulers.main)
                .doOnSubscribe {
                    currentState = currentState.copy(requestState = RequestState.LOADING)
                    _liveData.value = currentState
                }
                .subscribe(::updateSearchResult, ::handleError)
        )
    }

    fun viewState(): SearchViewState = currentState

    fun updateState(newState: SearchViewState) {
        currentState = newState
    }

    @MainThread
    private fun updateSearchResult(photos: List<FlickrPhoto>) {
        val ids = photos.map { it.id }
        val urls = photos.map { it.canonicalUrl() }
        val items = ids
            .zip(urls)
            .filter { (_, url) -> url.isNotEmpty() }
            .map { (id, url) -> PhotoItem(id, url) }
        currentState = currentState.copy(
            photoItems = items,
            requestState = RequestState.SUCCESS
        )
        _liveData.value = currentState
    }

    @MainThread
    private fun handleError(t: Throwable) {
        currentState = currentState.copy(
            error = t,
            requestState = RequestState.ERROR
        )
        _liveData.value = currentState
    }
}
