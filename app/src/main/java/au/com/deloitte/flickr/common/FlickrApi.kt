package au.com.deloitte.flickr.common

import au.com.deloitte.flickr.BuildConfig
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Query

interface FlickrApi {

    @GET("services/rest/?method=flickr.photos.search&format=json&nojsoncallback=1")
    fun searchPhotos(
        @Query("text") text: String,
        @Query("page") page: Int = 0,
        @Query("api_key") apiKey: String = BuildConfig.API_KEY
    ): Single<FlickrSearchResponse>
}