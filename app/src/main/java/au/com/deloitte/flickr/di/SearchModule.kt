package au.com.deloitte.flickr.di

import au.com.deloitte.flickr.common.FlickrApi
import au.com.deloitte.flickr.search.FlickrSearchRepository
import au.com.deloitte.flickr.search.SearchRepository
import dagger.Module
import dagger.Provides

@Module
object SearchModule {

    @SearchScope
    @JvmStatic
    @Provides
    fun provideSearchRepository(api: FlickrApi): SearchRepository = FlickrSearchRepository(api)
}