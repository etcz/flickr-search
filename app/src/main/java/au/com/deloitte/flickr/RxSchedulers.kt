package au.com.deloitte.flickr

import io.reactivex.Scheduler

data class RxSchedulers(
    val io: Scheduler,
    val computation: Scheduler,
    val main: Scheduler
)
