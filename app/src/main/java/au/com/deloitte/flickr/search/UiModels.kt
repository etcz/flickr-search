package au.com.deloitte.flickr.search

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class SearchViewState(
    val query: String = "",
    val photoItems: List<PhotoItem> = emptyList(),
    val error: Throwable? = null,
    val requestState: RequestState = RequestState.INITIAL
) : Parcelable

@Parcelize
data class PhotoItem(
    val id: String,
    val url: String
) : Parcelable

@Parcelize
enum class RequestState : Parcelable {
    INITIAL,
    LOADING,
    SUCCESS,
    ERROR
}
