package au.com.deloitte.flickr.search

import au.com.deloitte.flickr.common.FlickrPhoto
import io.reactivex.Single

interface SearchRepository {
    fun searchPhotos(text: String): Single<List<FlickrPhoto>>
}