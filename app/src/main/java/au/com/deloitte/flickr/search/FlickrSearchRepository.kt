package au.com.deloitte.flickr.search

import au.com.deloitte.flickr.common.FlickrApi
import au.com.deloitte.flickr.common.FlickrPhoto
import io.reactivex.Single
import javax.inject.Inject

class FlickrSearchRepository @Inject constructor(
    private val api: FlickrApi
) : SearchRepository {

    override fun searchPhotos(text: String): Single<List<FlickrPhoto>> {
        return api.searchPhotos(text = text).map {
            it.photos.photosList
        }
    }
}