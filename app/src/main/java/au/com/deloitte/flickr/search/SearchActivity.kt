package au.com.deloitte.flickr.search

import android.os.Bundle
import android.view.Menu
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import au.com.deloitte.flickr.*
import au.com.deloitte.flickr.di.DaggerAppComponent
import kotlinx.android.synthetic.main.activity_search.*
import javax.inject.Inject

class SearchActivity : AppCompatActivity(), SearchView.OnQueryTextListener {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory
    lateinit var viewModel: SearchViewModel
    lateinit var searchView: SearchView

    private var resultsAdapter: SearchResultsAdapter? = null
    private var currentQuery = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        injectDependencies()
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        viewModel = viewModelFrom(viewModelFactory)
        initViews()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        super.onCreateOptionsMenu(menu)
        menuInflater.inflate(R.menu.search_menu, menu)
        searchView = menu.findItem(R.id.action_search).actionView as SearchView
        searchView.setOnQueryTextListener(this)
        if (currentQuery.isNotEmpty()) {
            searchView.onActionViewExpanded()
            searchView.clearFocus()
            searchView.setQuery(currentQuery, false)
        }
        return true
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putParcelable(BUNDLE_VIEW_STATE, viewModel.viewState())
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        val previousState = savedInstanceState.getParcelable(BUNDLE_VIEW_STATE)
                as SearchViewState? ?: return
        currentQuery = previousState.query
        viewModel.updateState(previousState)
        render(previousState)
    }

    override fun onQueryTextChange(newText: String?) = true

    override fun onQueryTextSubmit(query: String?): Boolean {
        if (!query.isNullOrEmpty()) {
            submitQuery(query)
            searchView.clearFocus()
        }
        return true
    }

    private fun injectDependencies() {
        DaggerAppComponent.create()
            .searchComponent()
            .create()
            .inject(this)
    }

    private fun initViews() {
        val dividerDrawable = getDrawable(R.drawable.photo_list_divider)!!
        val horizontalDivider = DividerItemDecoration(
            this,
            DividerItemDecoration.HORIZONTAL
        ).apply {
            setDrawable(dividerDrawable)
        }
        val verticalDivider = DividerItemDecoration(
            this,
            DividerItemDecoration.VERTICAL
        ).apply {
            setDrawable(dividerDrawable)
        }
        val gridLayoutManager = GridLayoutManager(
            this,
            resources.getInteger(R.integer.search_results_columns),
            RecyclerView.VERTICAL,
            false
        )
        photosRecyclerView.apply {
            layoutManager = gridLayoutManager
            addItemDecoration(horizontalDivider)
            addItemDecoration(verticalDivider)
            setHasFixedSize(true)
        }
    }

    private fun render(viewState: SearchViewState) {
        when (viewState.requestState) {
            RequestState.INITIAL -> {
                photosRecyclerView.hide()
                emptyStateView.show()
            }
            RequestState.LOADING -> {
                emptyStateView.remove()
            }
            RequestState.SUCCESS -> {
                emptyStateView.remove()
                resultsAdapter = SearchResultsAdapter()
                resultsAdapter?.submitList(viewState.photoItems)
                photosRecyclerView.adapter = resultsAdapter
                photosRecyclerView.show()
            }
            RequestState.ERROR -> {
                emptyStateView.remove()
                shortToast(getString(R.string.generic_error_message))
            }
        }
    }

    private fun submitQuery(query: String) {
        currentQuery = query
        viewModel.searchPhotos(query)
        viewModel.liveData.removeObservers(this)
        viewModel.liveData.observe(this, Observer(::render))
    }

    companion object {
        const val BUNDLE_VIEW_STATE = "BUNDLE_VIEW_STATE"
    }
}
