package au.com.deloitte.flickr

import android.content.Context
import android.widget.Toast
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider

inline fun <reified VM : ViewModel> FragmentActivity.viewModelFrom(
    provider: ViewModelProvider.Factory
): VM {
    return ViewModelProvider(this, provider).get(VM::class.java)
}

fun Context.shortToast(text: String) {
    Toast.makeText(this, text, Toast.LENGTH_SHORT).show()
}
